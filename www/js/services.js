var _currentPage;
var _preview;
var _scope;
var _photoType;


angular.module('starter.services', [])
	.service('PhotoService', function($sce, $ionicSlideBoxDelegate, $timeout) {

		this.photoHolder = undefined;

		this.takePhoto = function(scope, photoType) {

			if (!this.photoHolder) return;
			_scope = scope;

			_currentPage = this.photoHolder.page[this.photoHolder.index];
			_preview = this.photoHolder.photoPreview;

			_scope.photo = _preview;

			var _photoType = Camera.PictureSourceType.CAMERA;
			if (photoType != undefined) {
				_photoType = photoType;
			}

			navigator.camera.getPicture(function(imageURI) {

				var parseFile = new Parse.File("mypic.jpg", {
					base64: imageURI
				});
				parseFile.save();
				_currentPage.parseImage = parseFile;
				_preview.preview = "data:image/jpeg;base64," + imageURI;
				// _currentPage.image = "data:image/jpeg;base64," + imageURI;
				_scope.openPreview();

			}, function(err) {
				console.log(err);

			}, {
				quality: 50,
				destinationType: navigator.camera.DestinationType.DATA_URL,
				sourceType: _photoType
			});
		}


		this.takeUserPhoto = function(scope, photoType, user) {

			if (!user) return;
			_scope = scope;

			var _photoType = Camera.PictureSourceType.CAMERA;
			if (photoType != undefined) {
				_photoType = photoType;
			}

			navigator.camera.getPicture(function(imageURI) {

				var parseFile = new Parse.File("mypic.jpg", {
					base64: imageURI
				});
				parseFile.save();
				// _scope.user.photo = parseFile;
				_scope.user.preview = "data:image/jpeg;base64," + imageURI;

			}, function(err) {
				console.log(err);

			}, {
				quality: 10,
				destinationType: navigator.camera.DestinationType.DATA_URL,
				sourceType: _photoType
			});
		}

	})

.service('AudioService', function() {
	this.recordAudio = function() {
		var captureSuccess = function(mediaFiles) {
			var i, path, len;
			for (i = 0, len = mediaFiles.length; i < len; i += 1) {
				path = mediaFiles[i].fullPath;
				// do something interesting with the file
				alert(path);
			}
		};

		// capture error callback
		var captureError = function(error) {
			navigator.notification.alert('Error code: ' + error.code, null, 'Capture Error');
		};

		// start audio capture
		navigator.device.capture.captureAudio(captureSuccess, captureError, {
			limit: 2
		});
	};
})

.service('StoriesService', function($sce) {

	this.storiesData = {
		data: [],
		dataClean: []
	};
	this.communityStoriesData = {
		data: [],
		dataClean: []
	};

	this.addStory = function(s) {

		var found = false;
		for (var i in this.storiesData.data) {
			if (this.storiesData.data[i].id == s.id) found = true;
		}

		if (!found) {
			this.storiesData.data.push({
				id: s.id,
				title: s.get('title'),
				date: new Date(s.createdAt),
				image: "img/cover2.jpg",
				pages: []
			});
		}

	}

	this.addPage = function(page) {

		var s = page.get("story");
		this.addStory(s);

		var found = false;
		for (var i in this.storiesData.data) {
			if (this.storiesData.data[i].id == s.id) {

				var story = this.storiesData.data[i];

				var img = page.get('image_file');
				if (img != undefined) {
					img = $sce.trustAsResourceUrl(page.get('image_file').url());
					story.image = img;
				}

				story.pages.push({
					text: page.get('text'),
					image_file: img,
					page: page.get('pageNumber'),
				});

			}
		}

	}

	this.addCommunityStory = function(s) {

		var found = false;
		for (var i in this.communityStoriesData.data) {
			if (this.communityStoriesData.data[i].id == s.id) found = true;
		}

		if (!found) {
			this.communityStoriesData.data.push({
				id: s.id,
				title: s.get('title'),
				date: new Date(s.createdAt),
				image: "img/cover2.jpg",
				pages: []
			});
		}

	}

	this.addCommunityPage = function(page) {

		var s = page.get("story");
		this.addCommunityStory(s);

		var found = false;
		for (var i in this.communityStoriesData.data) {
			if (this.communityStoriesData.data[i].id == s.id) {

				var story = this.communityStoriesData.data[i];

				var img = page.get('image_file');
				if (img != undefined) {
					img = $sce.trustAsResourceUrl(page.get('image_file').url());
					story.image = img;
				}

				story.pages.push({
					text: page.get('text'),
					image_file: img,
					page: page.get('pageNumber'),
				});

			}
		}

	}

})

;