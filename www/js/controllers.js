var cleanData = function(data) {
  if (data.length > 0) {

    var arr = angular.copy(data);
    var lengthofsublist = 2;

    var arrayToReturn = [];
    var subArray = [];
    var pushed = true;
    for (var i = 0; i < arr.length; i++) {
      if ((i + 1) % lengthofsublist == 0) {
        subArray.push(arr[i]);
        arrayToReturn.push(subArray);
        subArray = [];
        pushed = true;
      } else {
        subArray.push(arr[i]);
        pushed = false;
      }
    }
    if (!pushed)
      arrayToReturn.push(subArray);

    //console.log(JSON.stringify(arrayToReturn));
    return arrayToReturn;
  }
  return [];
}


angular.module('starter.controllers', [])
  .controller('BrowseCtrl', function($scope, $sce, StoriesService, $ionicHistory, $ionicNavBarDelegate, $ionicLoading, $timeout) {

    Parse.initialize("FO1z2Q1t0QQMv4uMxqyWb56rPCUZAczMPQCmxDvS", "y0aNbTx7pWRLvPm29uuImnZUz2plUBwA7QlitJLk");

    $scope.stories = StoriesService.storiesData;
    $scope.communityStories = StoriesService.communityStoriesData;

    var getStories = function() {

      console.log("getting stories...");

      $scope.stories.data = [];
      $scope.stories.dataClean = [];

      $scope.communityStories.data = [];
      $scope.communityStories.dataClean = [];

      console.log("getting stories for " + $scope.user.name);
      var Pages = Parse.Object.extend("Page");
      var query_pages = new Parse.Query(Pages);
      query_pages.include("story");
      query_pages.equalTo("user", $scope.user.name);
      query_pages.ascending("createdAt");
      query_pages.ascending("pageNumber");
      query_pages.find().then(function(pages) {

        for (var i = 0; i < pages.length; i++) {
          StoriesService.addPage(pages[i]);
        }
        $scope.stories.dataClean = cleanData($scope.stories.data);

      });

      var query_pages2 = new Parse.Query(Pages);
      query_pages2.include("story");
      query_pages2.ascending("createdAt");
      query_pages2.ascending("pageNumber");
      query_pages2.find().then(function(pages) {

        for (var i = 0; i < pages.length; i++) {
          StoriesService.addCommunityPage(pages[i]);
        }
        $scope.communityStories.dataClean = cleanData($scope.communityStories.data);

      });
    }

    //clear nav history
    $scope.$on('$ionicView.beforeEnter', function() {
      $ionicHistory.clearHistory();
      $ionicNavBarDelegate.showBackButton(false);

      // getStories();

    });
    $scope.$on('$ionicView.enter', function(){
      $scope.refresh();
    })
    console.log("Loading stories...");

    $scope.refresh = function() {
      getStories();
      $ionicLoading.show({
        template: '<ion-spinner icon="ripple" class="spinner-energized"></ion-spinner><br/>Loading the latest stories...'
      })
      $timeout(function() {
        $ionicLoading.hide();
      }, 1000)
    }


  })
  .controller('AppCtrl', function($scope, $ionicModal, $timeout, PhotoService, AudioService, $ionicSlideBoxDelegate, $ionicActionSheet, $ionicPlatform, $ionicHistory, $ionicPopup, $ionicScrollDelegate, $ionicLoading, $rootScope) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.showSidebar = true;
    $scope.editing = {
      active: false
    };
    $scope.newWordList = ["bat", "cat", "rat", "fan", "man", "pan", "cap", "map", "nap", "tap", "bag", "tag", "rag", "jam", "yam", "bad", "dad", "mad", "sad", "my", "all", "mum", "bus", "bed", "red", "jet", "net", "pet", "wet", "den", "hen", "pen", "ten", "beg", "leg", "sit", "big", "dig", "fig", "wig", "win", "zoo", "orange", "blue", "great", "family", "pink"];

    $scope.search = {};
    $scope.searchTerms = JSON.parse(window.localStorage['searchTerms'] || '{}');
    $rootScope.user = JSON.parse(window.localStorage['user'] || '{}');
    $scope.photo = {
      preview: '', 
      saveTo: ''
    };
    $scope.showList = false;
    $scope.showOverview = false;

    $ionicPlatform.registerBackButtonAction(
      function(event) {
        if ($ionicHistory.currentView().stateName == "home") {
          event.preventDefault();
        }
      }, 100);

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $ionicModal.fromTemplateUrl('templates/story.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.story = modal;
    });
    $ionicModal.fromTemplateUrl('templates/dict.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.dictModal = modal;
    });
    $ionicModal.fromTemplateUrl('templates/imagesave.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.previewModal = modal;
    });
    //preview modal for saving new photos to words
    $scope.closePreview = function() {
      $scope.previewModal.hide();
      $scope.photo.preview = '';

    };

    $scope.openPreview = function() {
      $ionicScrollDelegate.$getByHandle('saveScreen').scrollTop();
      $scope.previewModal.show();
    };

    //modal for adding images from MyDictionary
    $scope.closeDict = function() {
      $scope.dictModal.hide();
      $scope.search.text = "";
      $scope.showList = false;

    };

    $scope.openCategory = function() {
      $scope.showList = true;
    }

    // Open the login modal
    $scope.dict = function() {
      $scope.dictModal.show();
    };
    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      // window.localStorage['user'] = JSON.stringify($scope.user);
      $scope.modal.hide();
      // alert('closing');
    };

    // Open the login modal
    $scope.login = function() {
      window.localStorage['user'] = JSON.stringify($scope.user);
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function() {
        $scope.closeLogin();
      }, 1000);
    };



    $scope.current_story = {};
    $scope.openStory = function(story) {
      $ionicSlideBoxDelegate.$getByHandle('readStory').update();
      $scope.current_story = story;
      $timeout(function(){
        $scope.story.show();
        $ionicSlideBoxDelegate.$getByHandle('readStory').slide(0);
      $ionicSlideBoxDelegate.$getByHandle('readStory').update();
      }, 500)
      
    }

    $scope.closeStory = function() {
      $scope.story.hide();
      $scope.showOverview = false;
    }

    $scope.openOverview = function(){
      $scope.showOverview = true;
    }

    $scope.hideOverview = function(){
      $scope.showOverview = false;
    }

    $scope.takePhoto = function(stype) {
      PhotoService.takePhoto($scope, stype);
    }

    $scope.toSlide = function(index){
      $ionicSlideBoxDelegate.$getByHandle('readStory').slide(index);
    }

    $scope.insertImage = function() {
      PhotoService.photoHolder.page[PhotoService.photoHolder.index].image = $scope.photo.preview;
      console.log($scope.photo.preview);
      $ionicSlideBoxDelegate.$getByHandle('newStory').update();
      if($scope.photo.saveTo){
        $scope.addWord($scope.photo.saveTo);
      }
      
      $scope.closePreview();
    }

    $scope.recordAudio = function() {
      AudioService.recordAudio();
    }

    $scope.clearSearch = function() {
      $scope.search.text = "";
    }

    $scope.search = function() {
      var term = $scope.search.text.toLowerCase();

      // $scope.search.text = ""
      // alert(JSON.stringify($scope.searchTerms));
      $scope.addWord(term);
      if ($scope.searchTerms[term] == 5 || $scope.searchTerms[term] == 10) {
        $scope.popupFav(term, $scope.searchTerms[term]);
        console.log("congrats!");
      }
    }

    $scope.searchFromTerm = function(term) {
        $scope.search.text = term;
      }
      //add a word to the dictionary and save it to local storage
    $scope.addWord = function(term) {
      if ($scope.searchTerms && term in $scope.searchTerms) {
        $scope.searchTerms[term] ++;
        window.localStorage['searchTerms'] = JSON.stringify($scope.searchTerms);
        // $scope.search.text = ""
        // alert(JSON.stringify($scope.searchTerms));
      } else {
        $scope.searchTerms[term] = 1;
        window.localStorage['searchTerms'] = JSON.stringify($scope.searchTerms);
      }
    }

    //a popup shows when users use a word for certain amount of times
    $scope.popupFav = function(term, number) {
      var titleText = "";
      var subtitleText = "";
      if (number == 5) {
        titleText = 'Like using the word "' + term + '"?';
        subtitleText = 'I added a <i class="icon ion-heart"></i> to it in your dictionary';
      } else if (number == 10) {
        var newWord = $scope.newWordList.pop();
        var ri = Math.floor(Math.random() * $scope.newWordList.length);
        var newWord = $scope.newWordList[ri];
        $scope.addWord(newWord);
        titleText = 'You use the word"' + term + '"a lot';
        subtitleText = 'I added a new word "' + newWord + '" to your dictionary, go check it out!';

      }
      var favPopup = $ionicPopup.alert({
        title: titleText,
        subTitle: subtitleText,
        okText: 'Okay, thanks!',
        okType: 'button-energized'
      });
      favPopup.then(function(res) {
        console.log('Saved');
      });
    }

    $scope.deleteWord = function(key) {
      if (key in $scope.searchTerms) {
        delete $scope.searchTerms[key];
        window.localStorage['searchTerms'] = JSON.stringify($scope.searchTerms);
      }
    }

    $scope.photoOptions = function() {
      // Show the action sheet
      var hideSheet = $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-camera"></i> Take a new photo'
        }, {
          text: '<i class="icon ion-images"></i> Choose a existing photo'
        }],
        titleText: 'Add your own photo',
        cancelText: 'Cancel',
        cancel: function() {
          // add cancel code..
        },
        buttonClicked: function(index) {
          if (index == 0) {

            $scope.takePhoto(Camera.PictureSourceType.CAMERA);
          } else {
            $scope.takePhoto(Camera.PictureSourceType.PHOTOLIBRARY);
          }
          return true;
        }
      });

    }
    $scope.profileOptions = function() {
      // Show the action sheet
      var hideSheet = $ionicActionSheet.show({
        buttons: [{
          text: '<i class="icon ion-camera"></i> Take a new photo'
        }, {
          text: '<i class="icon ion-images"></i> Choose a existing photo'
        }],
        titleText: 'Add a photo of yourself',
        cancelText: 'Cancel',
        cancel: function() {
          // add cancel code..
        },
        buttonClicked: function(index) {
          if (index == 0) {
            PhotoService.takeUserPhoto($scope, Camera.PictureSourceType.CAMERA, $scope.user);
          } else {
            PhotoService.takeUserPhoto($scope, Camera.PictureSourceType.PHOTOLIBRARY, $scope.user);
            console.log(JSON.stringify($scope.user));
          }
          return true;
        }
      });

    }

    $scope.saveUser = function(){
      if ($scope.user.preview){
        $scope.user.image = $scope.user.preview;
        delete $scope.user['preview'];
      }
      // alert(JSON.stringify($scope.user));
      window.localStorage['user'] = JSON.stringify($scope.user);
      $ionicLoading.show({
        template: '<ion-spinner icon="ripple" class="spinner-energized"></ion-spinner><br/>Updating your profile...'
      })
      $timeout(function() {
        $ionicLoading.hide();
      }, 1000)
      $scope.closeLogin();
    }

  })
  .controller('PlaylistsCtrl', function($scope) {

  })
  .controller('NewCtrl', function($scope, $timeout, $ionicSlideBoxDelegate, PhotoService, AudioService, StoriesService, $ionicModal, $ionicPopup, $ionicLoading, $state, $ionicNavBarDelegate) {
    $scope.pages = [{}];
    $scope.sTitle = "New Story";

    $scope.showSug;
    $scope.$on('$ionicView.beforeEnter', function() {
      $ionicSlideBoxDelegate.$getByHandle('newStory').slide(0);
      PhotoService.photoHolder = {
        photoPreview: $scope.photo,
        page: $scope.pages,
        index: $ionicSlideBoxDelegate.$getByHandle('newStory').currentIndex()
      };
      $ionicNavBarDelegate.showBackButton(true);
      $scope.$parent.editing.active = true;
    });
    $scope.$on('$ionicView.enter', function() {
      $ionicSlideBoxDelegate.$getByHandle('newStory').update();
      PhotoService.photoHolder = {
        photoPreview: $scope.photo,
        page: $scope.pages,
        index: $ionicSlideBoxDelegate.$getByHandle('newStory').currentIndex()
      };
      $scope.showSug = false;
    });
    $scope.$on('$ionicView.beforeLeave', function() {
      $scope.pages = [{}];
      $scope.sTitle = "New Story";
      $scope.$parent.editing.active = false;
      PhotoService.photoHolder = undefined;
    });

    $scope.showPopup = function() {
      $scope.data = {
        title: "New Story"
      };
      // An elaborate, custom popup
      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="data.title"  onClick="this.select();">',
        title: 'Give your story a title',
        scope: $scope,
        buttons: [{
          text: 'Cancel'
        }, {
          text: '<b>Save</b>',
          type: 'button-energized',
          onTap: function(e) {
            if (!$scope.data.title) {
              //don't allow the user to close unless he enters wifi password
              $scope.data.title = "New Story";
            }
            // alert($scope.data.title);
            $scope.sTitle = $scope.data.title;
            $scope.saveStory();
          }
        }]
      });
      myPopup.then(function(res) {
        console.log('Tapped!', res);
      });

    };

    $scope.slideHasChanged = function(i) {
      PhotoService.photoHolder = {
        photoPreview: $scope.photo,
        page: $scope.pages,
        index: i
      };
    }

    $scope.useKeyword = function(key, page) {
      var tempText = page.text;
      if (tempText) {
        var words = tempText.split(" ");
      } else {
        var words = [];
      }
      //remove the last word if it's a suggestion
      //this doesn't work
      // if (/\s+$/.test(tempText)) {
      //   words.pop();
      // }
      words.push(key);
      page.text = words.join(" ");
      $scope.searchTerms[key] ++;
      if ($scope.searchTerms[key] == 5 || $scope.searchTerms[key] == 10) {
        console.log("congrats!");
        $scope.popupFav(key, $scope.searchTerms[key]);
      }
      $scope.showSug = true;
      $timeout(function() {
        document.getElementById("textInput").focus();
      });

    }

    $scope.typing = function() {
      $scope.showSug = true;
    }
    $scope.notTyping = function() {
      $timeout(function() {
        $scope.showSug = false;
      }, 500);
    }

    $scope.newPage = function() {
      $scope.pages.push({});
      $ionicSlideBoxDelegate.$getByHandle('newStory').update();
      $timeout(function() {
        $ionicSlideBoxDelegate.$getByHandle('newStory').slide($ionicSlideBoxDelegate.$getByHandle('newStory').slidesCount() - 1);
      }, 100);
    }

    var newStory;
    $scope.saveStory = function() {
      $ionicLoading.show({
        template: '<ion-spinner icon="ripple" class="spinner-energized"></ion-spinner><br/>Saving ' + $scope.sTitle + '...'
      })
      var NewStory = Parse.Object.extend("Story");
      newStory = new NewStory();

      var Page = Parse.Object.extend("Page");

      newStory.set("title", $scope.sTitle);
      newStory.set("user", $scope.user.name);
      // newStory.set("userimage", $scope.user.photo.getUrl());

      newStory.save(null, {
        success: function(newStory) {
          $scope.savedStory = newStory;
          // Execute any logic that should take place after the object is saved.
          console.log('New object created with objectId: ' + newStory.id);
          var pageCount = 0;
          for (var p in $scope.pages) {
            if ($scope.pages[p].text || $scope.pages[p].parseImage || $scope.pages[p].voice) {
              var newPage = new Page();
              newPage.set("story", newStory);
              newPage.set("pageNumber", pageCount + 1);
              pageCount++;
              newPage.set("image_file", $scope.pages[p].parseImage);
              newPage.set("text", $scope.pages[p].text);
              newPage.set("voice", $scope.pages[p].voice);
              newPage.set("user", $scope.user.name);
              newPage.save(null, {
                success: function(newPage) {
                  console.log(newPage.id);
                  StoriesService.storiesData.data.push({
                    id: newStory.id,
                    title: newStory.get('title'),
                    date: newStory.updatedAt,
                    pages: angular.copy($scope.pages)
                  });

                  StoriesService.storiesData.dataClean = cleanData(StoriesService.storiesData.data);

                },
                error: function() {}
              });
            }
          }
          if (pageCount == 0) {
            newStory.destroy({
              success: function() {},
              error: function() {}
            });
          }

          $ionicLoading.hide();
          var alertPopup = $ionicPopup.alert({
            title: 'Your story is saved!',
            okText: 'View story',
            okType: 'button-energized'
          });
          alertPopup.then(function(res) {
            $state.go('app.browse')
            console.log('Saved');
          });
        },
        error: function(gameScore, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          console.log('Failed to create new object, with error code: ' + error.message);
        }
      });
    }
  })

.controller('HomeCtrl', function($scope, $stateParams, $ionicSideMenuDelegate) {
    // console.log('hello');
    $ionicSideMenuDelegate.canDragContent(false);
    $scope.showSidebar = false;
  })
  .controller('PlaylistCtrl', function($scope, $stateParams) {});